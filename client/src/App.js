import logo from "./logo.svg";
import "./App.css";
import LocationSender from "./LocationService";

function App() {
  return (
    <div>
      <LocationSender />
    </div>
  );
}

export default App;
