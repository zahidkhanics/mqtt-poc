// import React, { useEffect } from "react";
// import { Client } from "paho-mqtt";

// const LocationSender = () => {
//   const handleSendLocation = () => {
//     const client = new Client(
//       "broker.emqx.io",
//       8083,
//       "/mqtt",
//       `mqttjs_${Math.random().toString(16).substr(2, 8)}`
//     );

//     client.onConnectionLost = (responseObject) => {
//       if (responseObject.errorCode !== 0) {
//         console.log(`Connection lost: ${responseObject.errorMessage}`);
//       }
//     };

//     client.connect({
//       useSSL: false,
//       onSuccess: () => {
//         const locationData = {
//           latitude: 40.7128,
//           longitude: -74.006,
//         };

//         client.send("driver/location", JSON.stringify(locationData));
//         console.log("Sent location:", locationData);
//         client.disconnect();
//       },
//       onFailure: (responseObject) => {
//         console.log(`Connection failed: ${responseObject.errorMessage}`);
//       },
//     });
//   };

//   useEffect(() => {}, []);

//   return (
//     <div>
//       <button onClick={handleSendLocation}>Send Location</button>
//     </div>
//   );
// };

// export default LocationSender;

import React, { useEffect } from "react";
import { Client } from "paho-mqtt";

const LocationSender = () => {
  useEffect(() => {
    const client = new Client(
      "broker.emqx.io",
      8083,
      "/mqtt",
      `mqttjs_${Math.random().toString(16).substr(2, 8)}`
    );

    const sendLocation = () => {
      const locationData = {
        latitude: Math.random() * 90,
        longitude: Math.random() * 180,
      };

      client.send("driver/location", JSON.stringify(locationData));
      console.log("Sent location:", locationData);
    };

    client.connect({
      useSSL: false,
      onSuccess: () => {
        console.log("Connected to MQTT broker");

        // Send location every second
        const intervalId = setInterval(sendLocation, 1000);

        return () => {
          clearInterval(intervalId);
          client.disconnect();
        };
      },
      onFailure: (responseObject) => {
        console.log(`Connection failed: ${responseObject.errorMessage}`);
      },
    });
  }, []);

  return (
    <div>
      <h1>Location Sender</h1>
      <p>Mocking automatic location updates</p>
    </div>
  );
};

export default LocationSender;
